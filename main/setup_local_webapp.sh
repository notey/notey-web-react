#!/bin/bash
BE_SERVER=${1:-172.16.1.20}
JETTY_HOME=$(jetty check | grep ^JETTY_HOME | cut -d= -f 2 | tr -d ' ')
if [ ! -d ${JETTY_HOME} ]; then
  printf "Cannot find Jetty installed\n\nRun 'brew install jetty76' and try again\n"
  exit 1
else 
  echo "Using Jetty in ${JETTY_HOME}"
fi
if [ ! -d ${JETTY_HOME}/webapps/ROOT ]; then
  echo "Creating ROOT webapp directory"
  mkdir ${JETTY_HOME}/webapps/ROOT
fi
echo "Copying webapp..."
if [ ! -d webapp ]; then
  echo "Cannot find webapp, did you intend to run from ${PWD}?"
  exit 1
fi
cp -rv webapp/ ${JETTY_HOME}/webapps/ROOT
echo "Installing java-lib"
cp -rv java-lib/ ${JETTY_HOME}/webapps/ROOT/WEB-INF/lib
echo Copying local settings to ${JETTY_HOME}
cp -rv resource/local/* ${JETTY_HOME}/webapps/ROOT
#echo Setting up Poca JS config
#cp -v resource/local/js/poca/config.js ${JETTY_HOME}/webapps/ROOT/js/poca/config.js
#echo Setting up FE Configuration for server http://$(hostname)
FILE=`mktemp -t jetty_install`; # Let the shell create a temporary file
trap 'rm -f $FILE' 0 1 2 3 15;   # Clean up the temporary file 
(
  echo 'cat <<END_OF_TEXT'
  cat "resource/local/WEB-INF/classes/config/fw_prop.properties"
  echo 'END_OF_TEXT'
) > $FILE
. $FILE > ${JETTY_HOME}/webapps/ROOT/WEB-INF/classes/config/fw_prop.properties
#echo "resource/local/WEB-INF/classes/config/fw_prop.properties -> ${JETTY_HOME}/webapps/ROOT/WEB-INF/classes/config/fw_prop.properties"
echo Installing Application Server JAR files from ${BE_SERVER}
curl -# -o ${JETTY_HOME}/webapps/ROOT/WEB-INF/lib/thelistapp.jar http://test2.notey.com/store/thelistapp.jar
curl -# -o ${JETTY_HOME}/webapps/ROOT/WEB-INF/lib/thelistweb.jar http://test2.notey.com/store/thelistweb.jar
if [ ! -d ${JETTY_HOME}/geoip ]; then
  echo "Creating GeoIP directory"
  mkdir ${JETTY_HOME}/geoip
fi
echo Installing GeoIP DB from ${BE_SERVER}
curl -# -o ${JETTY_HOME}/geoip/GeoLite2-City.mmdb http://test2.notey.com/store/GeoLite2-City.mmdb
cat << EOF > ngulp
#!/bin/bash
export NOTEY_HOME=${JETTY_HOME}/webapps/ROOT
gulp "\$@"
EOF
chmod +x ngulp 
./ngulp deploy-local
echo Now install gulp, and run 'npm install' to install the dependencies
echo Run ./ngulp for local gulp commands
