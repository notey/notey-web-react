<?xml version="1.0" encoding="utf-8"?>

<!--

CacheManager Configuration
==========================
An ehcache.xml corresponds to a single CacheManager.

See instructions below or the ehcache schema (ehcache.xsd) on how to configure.

System property tokens can be specified in this file which are replaced when the configuration
is loaded. For example multicastGroupPort=${multicastGroupPort} can be replaced with the
System property either from an environment variable or a system property specified with a
command line switch such as -DmulticastGroupPort=4446. Another example, useful for Terracotta
server based deployments is <terracottaConfig url="${serverAndPort}"/ and specify a command line
switch of -Dserver36:9510

The attributes of <ehcache> are:
* name - an optional name for the CacheManager.  The name is optional and primarily used
for documentation or to distinguish Terracotta clustered cache state.  With Terracotta
clustered caches, a combination of CacheManager name and cache name uniquely identify a
particular cache store in the Terracotta clustered memory.
* updateCheck - an optional boolean flag specifying whether this CacheManager should check
for new versions of Ehcache over the Internet.  If not specified, updateCheck="true".
* dynamicConfig - an optional setting that can be used to disable dynamic configuration of caches
associated with this CacheManager.  By default this is set to true - i.e. dynamic configuration
is enabled.  Dynamically configurable caches can have their TTI, TTL and maximum disk and
in-memory capacity changed at runtime through the cache's configuration object.
* monitoring - an optional setting that determines whether the CacheManager should
automatically register the SampledCacheMBean with the system MBean server.

Currently, this monitoring is only useful when using Terracotta clustering and using the
Terracotta Developer Console. With the "autodetect" value, the presence of Terracotta clustering
will be detected and monitoring, via the Developer Console, will be enabled. Other allowed values
are "on" and "off".  The default is "autodetect". This setting does not perform any function when
used with JMX monitors.
-->
<ehcache xsi:noNamespaceSchemaLocation="ehcache.xsd" 
	updateCheck="false" 
	monitoring="off" 
	dynamicConfig="false"
>
	<!--

	DiskStore configuration
	=======================

	The diskStore element is optional. To turn off disk store path creation, comment out the diskStore
	element below.

	Configure it if you have overflowToDisk or diskPersistent enabled for any cache.

	If it is not configured, and a cache is created which requires a disk store, a warning will be
	 issued and java.io.tmpdir will automatically be used.

	diskStore has only one attribute - "path". It is the path to the directory where
	.data and .index files will be created.

	If the path is one of the following Java System Property it is replaced by its value in the
	running VM. For backward compatibility these should be specified without being enclosed in the ${token}
	replacement syntax.

	The following properties are translated:
	* user.home - User's home directory
	* user.dir - User's current working directory
	* java.io.tmpdir - Default temp file path
	* ehcache.disk.store.dir - A system property you would normally specify on the command line
	  e.g. java -Dehcache.disk.store.dir=/u01/myapp/diskdir ...

	Subdirectories can be specified below the property e.g. java.io.tmpdir/one
	
	-->
	<diskStore path="java.io.tmpdir"/>
	
	
	<!--

	Cache configuration
	===================

	The following attributes are required.

	name:
	Sets the name of the cache. This is used to identify the cache. It must be unique.

	maxElementsInMemory:
	Sets the maximum number of objects that will be created in memory.  0 = no limit.
	In practice no limit means Integer.MAX_SIZE (2147483647) unless the cache is distributed
	with a Terracotta server in which case it is limited by resources.

	maxElementsOnDisk:
	Sets the maximum number of objects that will be maintained in the DiskStore
	The default value is zero, meaning unlimited.

	eternal:
	Sets whether elements are eternal. If eternal,  timeouts are ignored and the
	element is never expired.

	overflowToDisk:
	Sets whether elements can overflow to disk when the memory store
	has reached the maxInMemory limit.

	The following attributes and elements are optional.

	overflowToOffHeap:
	(boolean) This feature is available only in enterprise versions of Ehcache.
	When set to true, enables the cache to utilize off-heap memory
	storage to improve performance. Off-heap memory is not subject to Java
	GC. The default value is false.
	
	maxMemoryOffHeap:
	(string) This feature is available only in enterprise versions of Ehcache.
	Sets the amount of off-heap memory available to the cache.
	This attribute's values are given as <number>k|K|m|M|g|G|t|T for
	kilobytes (k|K), megabytes (m|M), gigabytes (g|G), or terabytes
	(t|T). For example, maxMemoryOffHeap="2g" allots 2 gigabytes to
	off-heap memory.

	This setting is in effect only if overflowToOffHeap is true.

	Note that it is recommended to set maxElementsInMemory to at least 100 elements
	when using an off-heap store, otherwise performance will be seriously degraded,
	and a warning will be logged.

	The minimum amount that can be allocated is 128MB. There is no maximum.

	timeToIdleSeconds:
	Sets the time to idle for an element before it expires.
	i.e. The maximum amount of time between accesses before an element expires
	Is only used if the element is not eternal.
	Optional attribute. A value of 0 means that an Element can idle for infinity.
	The default value is 0.

	timeToLiveSeconds:
	Sets the time to live for an element before it expires.
	i.e. The maximum time between creation time and when an element expires.
	Is only used if the element is not eternal.
	Optional attribute. A value of 0 means that and Element can live for infinity.
	The default value is 0.

	diskPersistent:
	Whether the disk store persists between restarts of the Virtual Machine.
	The default value is false.

	diskExpiryThreadIntervalSeconds:
	The number of seconds between runs of the disk expiry thread. The default value
	is 120 seconds.

	diskSpoolBufferSizeMB:
	This is the size to allocate the DiskStore for a spool buffer. Writes are made
	to this area and then asynchronously written to disk. The default size is 30MB.
	Each spool buffer is used only by its cache. If you get OutOfMemory errors consider
	lowering this value. To improve DiskStore performance consider increasing it. Trace level
	logging in the DiskStore will show if put back ups are occurring.

	clearOnFlush:
	whether the MemoryStore should be cleared when flush() is called on the cache.
	By default, this is true i.e. the MemoryStore is cleared.

	statistics:
	Whether to collect statistics. Note that this should be turned on if you are using
	the Ehcache Monitor. By default statistics is turned off to favour raw performance.
	To enable set statistics="true"

	memoryStoreEvictionPolicy:
	Policy would be enforced upon reaching the maxElementsInMemory limit. Default
	policy is Least Recently Used (specified as LRU). Other policies available -
	First In First Out (specified as FIFO) and Less Frequently Used
	(specified as LFU)

	copyOnRead:
	Whether an Element is copied when being read from a cache.
	By default this is false.

	copyOnWrite:
	Whether an Element is copied when being added to the cache.
	By default this is false.

	Cache elements can also contain sub elements which take the same format of a factory class
	and properties. Defined sub-elements are:

	* cacheEventListenerFactory - Enables registration of listeners for cache events, such as
	  put, remove, update, and expire.

	* bootstrapCacheLoaderFactory - Specifies a BootstrapCacheLoader, which is called by a
	  cache on initialisation to prepopulate itself.

	* cacheExtensionFactory - Specifies a CacheExtension, a generic mechanism to tie a class
	  which holds a reference to a cache to the cache lifecycle.

	* cacheExceptionHandlerFactory - Specifies a CacheExceptionHandler, which is called when
	  cache exceptions occur.

	* cacheLoaderFactory - Specifies a CacheLoader, which can be used both asynchronously and
	  synchronously to load objects into a cache. More than one cacheLoaderFactory element
	  can be added, in which case the loaders form a chain which are executed in order. If a
	  loader returns null, the next in chain is called.

	* copyStrategy - Specifies a fully qualified class which implements
	  net.sf.ehcache.store.compound.CopyStrategy. This strategy will be used for copyOnRead
	  and copyOnWrite in place of the default which is serialization.

	-->
	<defaultCache
		maxElementsInMemory="1000"
		eternal="false"
		timeToLiveSeconds="7200"
		overflowToDisk="false"
	/>
	
	<cache name="notey.topicDapImpl.findUserFavoriteTopics"
		maxElementsInMemory="1000"
		eternal="false"
		timeToLiveSeconds="7200"
		overflowToDisk="false"
	/>
	
	
</ehcache>