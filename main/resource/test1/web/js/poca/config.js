config = {
	hostDomain: location.origin,
	loadNewPostInterval: 60000,
	useLogger: true,
	useInScreenLogger: true,
//	signupPath: location.origin.replace(/http:/, 'https:') + '/api2/account/signup',
//	loginPath: location.origin.replace(/http:/, 'https:') + '/api/auth/login/p.json'
	signupPath: '/api2/account/signup',
	loginPath: '/api/auth/login/p.json'
};