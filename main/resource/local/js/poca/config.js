config = {
	hostDomain: location.origin,
	loadNewPostInterval: 10000,
	useLogger: true,
	useInScreenLogger: true,
	signupPath: '/api2/account/signup',
	loginPath: '/api/auth/login/p.json'
};
