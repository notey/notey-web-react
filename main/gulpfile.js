/**
 * Install gulp globally first: sudo npm install -g gulp
 * Install npm modules: npm install
 */

/**
 * COMMANDS
 *
 * Watch .scss files for changes: gulp dev
 * Watch .scss files for changes and sync to server: gulp dev --server YES
 *
 * Generated build folder: gulp build
 * Clean build folder: gulp clean-build
 *
 * Sync webapp to server: gulp sync
 * Sync build folder to server: gulp sync-build
 */

var gulp 		= require('gulp'),
	usemin 		= require('gulp-usemin'),
	sass 		= require('gulp-sass'),
	rimraf		= require('gulp-rimraf'),
	minifyCSS	= require('gulp-minify-css'),
	rename		= require('gulp-rename'),
	watch 		= require('gulp-watch'),
	uglify		= require('gulp-uglify'),
	concat 		= require('gulp-concat'),
	util 		= require('gulp-util'),
	replace 	= require('gulp-replace-task'),
	jshint 		= require('gulp-jshint'),
	tap 		= require('gulp-tap'),

	chalk		= require('chalk'),
	sequence	= require('run-sequence'),
	glob 		= require('glob'),
	path 		= require('path'),
	fs 		= require('node-fs'),
	rsync 		= require("rsyncwrapper").rsync;

/*---------------------------*\
	VARIABLES
\*---------------------------*/
var sourceDir = 'webapp',
    buildDir = 'build',
    serverLocation = process.env.NOTEY_HOME;

/*---------------------------*\
	MAIN TASKS
\*---------------------------*/
gulp.task('default', function() {
})

// gulp dev
// gulp dev --server YES
gulp.task('dev', function() {
	// Generate sass file, clean the .css-cache folder and copy all files to the server
	console.log(chalk.gray('Started watching for changes...'));
	if(util.env.server === 'YES') {
		gulp.watch(sourceDir + '/css/scss/**/*.*', function() {
			sequence('sass', 'clean-css-cache', 'copy-css-to-server');
		});
		gulp.watch(sourceDir + '/css/scss/**/*.css', ['copy-css-to-server']);
		gulp.watch([sourceDir + '/*.html'],['copy-html-to-server']);
		gulp.watch([sourceDir + '/WEB-INF/templates/**/*.html'],['copy-templates-to-server']);
		gulp.watch([sourceDir + '/templates-hidden/**/*.*'],['copy-templates-hidden-to-server']);
		gulp.watch([sourceDir + '/js/**/*.js'],['copy-js-to-server']);
		gulp.watch([sourceDir + '/images/**/*.*'],['copy-images-to-server']);
	} else {
		gulp.watch(sourceDir + '/css/scss/**/*.*', function() {
			sequence('sass', 'clean-css-cache');
		});
	}
})

gulp.task('deploy-local', function() {
	console.log(chalk.gray('Deploying to local jetty at ' + serverLocation));
	return sequence	(
			'sass',
			'clean-css-cache',
			'copy-css-to-server',
			'copy-html-to-server',
			'copy-templates-to-server',
			'copy-templates-hidden-to-server',
			'copy-js-to-server',
			'copy-images-to-server'
			);
})



// gulp build
gulp.task('build', function(cb) {
	console.log(chalk.grey('Starting build sequence...'));
	return sequence(
			// 'lint',
			[
				'clean-build',
				'clean-css'
			],
			'sass',
			'copy-build',
			'usemin',
			'minify-js',
			'clean-css-cache', cb);
})

/*---------------------------*\
	WATCH
\*---------------------------*/
gulp.task('watch-css', function(cb) {
	console.log(chalk.gray('Started watching for sass changes...'));
	gulp.watch(sourceDir + '/css/scss/**/*.*', ['sass', 'clean-css-cache']);
})

/*---------------------------*\
	USEMIN
\*---------------------------*/
gulp.task('usemin', function(cb) {
	console.log(chalk.gray('Combining javascript files...'));
	return sequence(
			'usemin-root',
			'usemin-fragment',
			cb
		)
})

gulp.task('usemin-root', function(cb) {
	return gulp.src(buildDir + '/*.html')
				.pipe(usemin({
					assetsDir: sourceDir + '/'
				}))
				.pipe(replace({
					patterns: [
						{
							match: /<script src="(.+)\.js".*?><\/script>/gi,
							replacement: '<script src="$1.min.js" class="lift:with-resource-id"></script>'
						},
						{
							match: /<link rel="stylesheet" href=\s*"([^"]+?)".*?\/>/gi,
							replacement: '<link rel="stylesheet" href="$1" class="lift:with-resource-id"/>'
						}
					]
				}))
				.pipe(gulp.dest(buildDir));
})

gulp.task('usemin-fragment', function(cb) {
	return gulp.src(buildDir + '/templates-hidden/fragment/*.html')
				.pipe(usemin({
					assetsDir: sourceDir + '/',
					outputRelativePath: '../../',
					js: []
				}))
				.pipe(replace({
					patterns: [
						{
							match: /<script src="(.+)\.js".*?><\/script>/gi,
							replacement: '<script src="$1.min.js" class="lift:with-resource-id"></script>'
						},
						{
							match: /<link rel="stylesheet" href=\s*"([^"]+?)".*?\/>/gi,
							replacement: '<link rel="stylesheet" href="$1" class="lift:with-resource-id" />'
						}
					]
				}))
				.pipe(gulp.dest(buildDir + '/templates-hidden/fragment'));
})

/*---------------------------*\
	COPY
\*---------------------------*/
gulp.task('copy-build', function(cb) {
	// Copy all files needed to start the build
	console.log(chalk.gray('Copying build files...'))
	return sequence(
				'copy-html',
				//'copy-images',
				'copy-css',
				'copy-css-fixes',
				'copy-templates-hidden',
				//'copy-fonts',
				//'copy-ckeditor',
				cb
			);
})

gulp.task('copy-html', function(cb) {
	// Copy html files
	console.log(chalk.grey('Copying html files...'))
	return gulp.src(sourceDir + '/*.html')
				.pipe(gulp.dest(buildDir));
})

gulp.task('copy-templates', function(cb) {
	// Copying template files
	console.log(chalk.grey('Copying template files...'))
	return gulp.src(sourceDir + '/WEB-INF/templates/**/*.html')
				.pipe(gulp.dest(buildDir) + '/WEB-INF/templates');
})

gulp.task('copy-images', function(cb) {
	// Copy image folder
	console.log(chalk.grey('Copying images...'));
	return gulp.src(sourceDir + '/images/**/*.*')
				.pipe(gulp.dest(buildDir + '/images'))
})

gulp.task('copy-fonts', function(cb) {
	// Copy fonts folder
	console.log(chalk.grey('Copying fonts...'));
	return gulp.src(sourceDir + '/font/**/*.*')
				.pipe(gulp.dest(buildDir + '/font'));
})

gulp.task('copy-css', function(cb) {
	// Copy css files
	console.log(chalk.grey('Copying css files...'));
	return gulp.src([
					sourceDir + '/css/notey.min.css',
					sourceDir + '/css/notey_app.min.css',
					sourceDir + '/css/theme_notey3.css'
				])
				.pipe(gulp.dest(buildDir + '/css'))
})

gulp.task('copy-css-fixes', function(cb) {
	// Copy css fixes files
	console.log(chalk.grey('Copying css fixes files...'));
	return gulp.src(sourceDir + '/css/fixes/*.css')
				.pipe(gulp.dest(buildDir + '/css/fixes/'));
})

gulp.task('copy-templates-hidden', function(cb) {
	// Copy templates-hidden folder
	console.log(chalk.grey('copying templates-hidden...'));
	return gulp.src(sourceDir + '/templates-hidden/**/*.*')
				.pipe(gulp.dest(buildDir + '/templates-hidden'));
})

gulp.task('copy-ckeditor', function(cb) {
	// Copy ckeditor folder
	console.log(chalk.grey('Copying ckeditor...'));
	return gulp.src(sourceDir + '/ckeditor/**/*.*')
				.pipe(gulp.dest(buildDir + '/ckeditor'))
})

gulp.task('copy-css-to-server', function(cb) {
	// Copy generated sass files to the server
	console.log(chalk.grey('Copying css files to server...'))
	return gulp.src([
					sourceDir + '/css/**/*.css',
				])
				.pipe(gulp.dest(serverLocation + '/css'));
})

gulp.task('copy-html-to-server', function(cb) {
	console.log(chalk.grey('Copying HTML files to server...'))
	return gulp.src([
					sourceDir + '/*.html'
				])
				.pipe(gulp.dest(serverLocation + '/'));
})

gulp.task('copy-templates-to-server', function(cb) {
	console.log(chalk.grey('Copying templates files to server...'))
	return gulp.src([
					sourceDir + '/WEB-INF/templates/**/*.html'
				])
				.pipe(gulp.dest(serverLocation + '/WEB-INF/templates'));
})

gulp.task('copy-images-to-server', function(cb) {
	console.log(chalk.grey('Copying images files to server...'))
	return gulp.src([
					sourceDir + '/images/**/*.*'
				])
				.pipe(gulp.dest(serverLocation + '/images'));
})

gulp.task('copy-templates-hidden-to-server', function(cb) {
	console.log(chalk.grey('Copying templates-hidden files to server...'))
	return gulp.src([
					sourceDir + '/templates-hidden/**/*.*'
				])
				.pipe(gulp.dest(serverLocation + '/templates-hidden'));
})

gulp.task('copy-js-to-server', function(cb) {
	console.log(chalk.grey('Copying Javascript files to server...'));
	return gulp.src([
					sourceDir + '/js/**/*.js',
					//'!' + sourceDir + '/js/poca/component/header/*.js',
					'!' + sourceDir + '/js/jquery/**',
					'!' + sourceDir + '/js/LL/**',
					'!' + sourceDir + '/js/zxcvbn.js',
					'!' + sourceDir + '/js/prefixfree.min.js',
					'!' + sourceDir + '/js/swfobject.js'
				])
				/* .pipe(tap(function (file, t) {
					console.log(file.path);
				})) */ //No need debug
				.pipe(gulp.dest(serverLocation + '/js'));
})


/*---------------------------*\
	SASS
\*---------------------------*/
gulp.task('sass', function(cb) {
	// Generate css files from our main .scss files
	console.log(chalk.grey('Generating css files...'));
	return gulp.src([
					sourceDir + '/css/scss/notey.scss',
					sourceDir + '/css/scss/notey_app.scss'
				])
                                .pipe(sass())
				.pipe(minifyCSS())
				.pipe(rename({
					extname: '.min.css'
				}))
				.pipe(gulp.dest(sourceDir + '/css'));
})

/*---------------------------*\
	MINIFY JAVASCRIPT
\*---------------------------*/
gulp.task('minify-js', function(cb) {
	// Minify javascript
	console.log(chalk.grey('Minifying javascript...'));
	var files = glob.sync(buildDir + '/js/**/*.js'),
		streams;

	streams = files.map(function(file) {
		return gulp.src(file)
					.pipe(uglify())
					.pipe(rename({
						extname: '.min.js'
					}))
					.pipe(gulp.dest(path.dirname(file)));
	})
})

/*---------------------------*\
	CONCACT JAVASCRIPT
\*---------------------------*/
gulp.task('concat-js', function(cb) {
	// Combine all javascript files
	console.log(chalk.grey('Combining javascript files...'));
	return gulp.src(sourceDir + '/js/**/*.js')
				.pipe(concat('all.js'))
				.pipe(gulp.dest(buildDir + '/js'));
})

/*---------------------------*\
	SYNC FOLDER
\*---------------------------*/
gulp.task('sync', function(cb) {
	console.log(chalk.grey('Syncing files to server...'));
	return rsync({
				src: sourceDir + '/',
				dest: serverLocation,
				recursive: true,
				syncDestIgnoreExcl: true,
				compareMode: 'checksum',
				exclude: [
					'build/',
					'M/',
					'node_modules/',
					'generated/',
					'WEB-INF/'
				],
				onStdout: function(data) {
					console.log(data);
				}
			}, function(error, stdout, stderr, cmd) {
				if(error) console.log(error.message);
			})
})

gulp.task('sync-build', function(cb) {
	console.log(chalk.grey('Syncing build folder to server...'));
	return rsync({
				src: buildDir + '/',
				dest: serverLocation,
				recursive: true,
				syncDestIgnoreExcl: true,
				compareMode: 'checksum',
				exclude: [
					'WEB-INF/'
				],
				onStdout: function(data) {
					console.log(data);
				}
			}, function(error, stdout, stderr, cmd) {
				if(error) console.log(error.message);
			})
})

/*---------------------------*\
	CREATE .HTACCESS
\*---------------------------*/
gulp.task('htaccess', function(cb) {
	// Create .htaccess file
	console.log(chalk.grey('Generating .htaccess file...'));
	return fs.writeFile(buildDir + '/.htaccess', 'RewriteEngine On\n\nRewriteCond %{REQUEST_URI}\nRewriteRule ^(.*)$ /build/$1 [L,R=301]');
})

/*---------------------------*\
	CLEAN
\*---------------------------*/
gulp.task('clean-build', function(cb) {
	// Clean the build folder
	console.log(chalk.grey('Cleaning build folder...'));
	return gulp.src(buildDir, {read: false})
				.pipe(rimraf());
})

gulp.task('clean-css', function(cb) {
	// Clean generated CSS files
	console.log(chalk.grey('Cleaning generated css files...'));
	return gulp.src([
					sourceDir + '/css/notey.min.css',
					sourceDir + '/css/notey_app.min.css'
				], {read: false})
				.pipe(rimraf())
})

gulp.task('clean-css-cache', function(cb) {
	// Clean up the .sass-cache
	console.log(chalk.grey('Cleaning .sass-cache...'));
	return gulp.src(sourceDir + '/.sass-cache', {read: false})
				.pipe(rimraf());
})

/*---------------------------*\
	JSHINT
\*---------------------------*/
gulp.task('lint', function(cb) {
	return gulp.src([
					sourceDir + '/js/**/*.js',
					'!' + sourceDir + '/js/poca/component/header/*.js',
					'!' + sourceDir + '/js/jquery/**',
					'!' + sourceDir + '/js/LL/**',
					'!' + sourceDir + '/js/zxcvbn.js',
					'!' + sourceDir + '/js/prefixfree.min.js',
					'!' + sourceDir + '/js/swfobject.js'
				])
				.pipe(jshint())
				.pipe(jshint.reporter('default'));
})
