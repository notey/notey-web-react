#!/bin/bash
FILE_NAME=`date +%Y%m%d-%H%M-notey-$3.zip`
GIT_HASH=`git -C $1 log -1 --format="%H"`
mkdir .aws_code_deploy
cp -R $1/main/* .aws_code_deploy/
cd .aws_code_deploy
npm install
gulp build
rsync -am --include='*.min.js' --include='*/' --exclude='*' webapp/ build/
rsync -am webapp/css/lib build/css/
rm -rf webapp/WEB-INF/lib/*
rm -rf node_modules
cp -R ../geoip .
cp $2 java-lib/
rsync -am  resource/aws/web/* webapp/ 
echo aws deploy push --application-name notey-web --description "Deploy Prod Build From ${GIT_HASH}" --ignore-hidden-files --s3-location s3://notey-code-deploy/$FILE_NAME --source . --profile notey --output json
aws deploy push --application-name notey-web --description "Deploy Prod Build From ${GIT_HASH}" --ignore-hidden-files --s3-location s3://notey-code-deploy/$FILE_NAME --source . --profile notey --output json
